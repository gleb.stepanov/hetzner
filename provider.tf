variable "hcloud_token" {}

# Configure the Hetzner Cloud Provider
provider "hcloud" {
  token = "${var.hcloud_token}"
}

#  Main ssh key
resource "hcloud_ssh_key" "default" {
  name       = "main ssh key"
  public_key = "${file("~/.ssh/id_rsa.pub")}"
}
